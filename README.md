# 2DPuzzle #

A simple/advanced game project. A 2D puzzle game which is not designed for performance or fancy graphics, but for fun in development. Overall goals:

* In-game level editor. Anything bundled with the game can be made and shared with the same tools.
* Multiplayer support
* Modding support, as in adding new components and features through DLLs

We'll be using as much .NET features as possible, OpenTK for graphics, sound, and windowing.

As a 2D puzzle game, there will be tiles. These tiles can span several units. They will probably need a spatial index once that is implemented.

There might be some entities. Each type will have its own class that is a subclass of Entity. It will somehow be tied up to a factory method.

# General coding standards #
* Private and protected members use an underscore postfix (_privateMember)
* Member and local variable names are written in camelback (variableName)
* Class names are written in camelcase (ClassName)
* Only allow access to what is necessary to allow access to. Consider what class should responsible for what and execute it so.
* Don't write performance-oriented. Do what is easy
* Use standard functions as much as possible
* Use OpenTK classes and functions as much as possible
* Avoid monolithic classes
* Write self-documenting code by using descriptive function names and ensure functions don't do too much alone
* Never directly interfere with OpenGL, especially not the OpenGL state (glEnable/glDisable/glBind). If something needs to be interfered with, implement it in CSGraphics.