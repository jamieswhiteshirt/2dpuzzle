#version 330

uniform sampler2D texture;

layout(location = 0) out vec4 out_Color;

in vec2 tex_coord;
in vec4 color;

void main()
{
	out_Color = texture2D(texture, tex_coord) * color;
}