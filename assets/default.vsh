#version 330

uniform mat4 modelview;
uniform mat4 projection;

layout(location = 0) in vec4 in_Vertex;
layout(location = 1) in vec4 in_Color;
layout(location = 2) in vec2 in_TexCoord;

out vec2 tex_coord;
out vec4 color;

void main()
{
	gl_Position = in_Vertex * modelview * projection;
	tex_coord = in_TexCoord;
	color = in_Color;
}