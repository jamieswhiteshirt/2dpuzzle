﻿using CSGraphics;
using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Main;
using Main.Utility;
using Main.Render;

namespace Entity
{
    public class EntityBase
    {
        public readonly int entityID;
        public World world;

        public DoubleBuffered<Vector2> position;
        public DoubleBuffered<Vector2> velocity;
        public float maxVelocity;

        private string _textureName;
        private Random _random = new Random();

        public EntityBase(World world, Vector2 position)
        {
            this.world = world;
            this.entityID = world.getNewEntityID(this);

            this.position = new DoubleBuffered<Vector2>(position);
            this.velocity = new DoubleBuffered<Vector2>(new Vector2());
            this.maxVelocity = 100;
        }

        public EntityBase setPosition(float x, float y)
        {
            this.position[0] = new Vector2(x, y);
            return this;
        }
        
        public EntityBase setPosition(Vector2 position)
        {
            this.position[0] = position;
            return this;
        }

        public EntityBase setMaxVelocity(float maxVelocity)
        {
            this.maxVelocity = maxVelocity;
            return this;
        }

        public EntityBase setVelocity(float x, float y)
        {
            this.velocity[0] = new Vector2(x, y);
            return this;
        }
        
        public EntityBase setVelocity(Vector2 velocity)
        {
            this.velocity[0] = velocity;
            return this;
        }

        public EntityBase setTextureName(string textureName)
        {
            this._textureName = textureName;
            return this;
        }

        public virtual void update()
        {
            //Swap the values so that the previous tick's values are put at index 1
            position.swap();
            velocity.swap();

            //Set the current tick's values to the previous tick's values
            position[0] = position[1];
            velocity[0] = velocity[1];

            float xOffset = ((float)_random.NextDouble() - (float)_random.NextDouble()) / 1000.0f;
            float yOffset = ((float)_random.NextDouble() - (float)_random.NextDouble()) / 1000.0f;
            velocity[0] -= new Vector2(xOffset, yOffset);

            position[0] += velocity[0];
        }

        //TODO: Actually remove this method. Entities should not have any connection to how it is rendered, but the other way around.
        //Needs some kind of type lookup with a factory method
        public Renderer getRenderer()
        {
            return new SimpleMeshRenderer(this.position, CommonShaderPrograms.texturedColored, CommonMeshes.square, Main.Program.resourceLoader.getTexture(_textureName));
        }
    }
}

