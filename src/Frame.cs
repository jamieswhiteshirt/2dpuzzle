using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    /// <summary>
    /// An interface for game logic and something that is to be displayed on the whole screen. Has a logic tick and a graphics tick.
    /// </summary>
    public interface Frame
    {
        /// <summary>
        /// Invoked when the Frame is added to the frame stack.
        /// </summary>
        void load();

        /// <summary>
        /// Invoked when the Frame is removed from the frame stack.
        /// </summary>
        void unload();

        /// <summary>
        /// Update tick.
        /// </summary>
        void update();

        /// <summary>
        /// Render tick.
        /// </summary>
        void render();
    }
}