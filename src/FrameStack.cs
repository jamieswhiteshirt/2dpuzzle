﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    public class FrameStack
    {
        private abstract class Action
        {
            public abstract void invoke(FrameStack stack);

            public class Push : Action
            {
                private Frame _frame;

                public Push(Frame frame)
                {
                    _frame = frame;
                }

                public override void invoke(FrameStack stack)
                {
                    stack._stack.Add(_frame);
                    _frame.load();
                }
            }

            public class Pop : Action
            {
                public override void invoke(FrameStack stack)
                {
                    stack._stack[stack._stack.Count - 1].unload();
                    stack._stack.RemoveAt(stack._stack.Count - 1);
                }
            }

            public class Clear : Action
            {
                public override void invoke(FrameStack stack)
                {
                    while (stack._stack.Count > 0)
                    {
                        stack._stack[stack._stack.Count - 1].unload();
                        stack._stack.RemoveAt(stack._stack.Count - 1);
                    }
                }
            }
        }

        private Queue<Action> _actionQueue = new Queue<Action>();
        private List<Frame> _stack = new List<Frame>();

        public FrameStack()
        { }

        public void update()
        {
            while (_actionQueue.Count > 0)
            {
                _actionQueue.Dequeue().invoke(this);
            }

            foreach(Frame frame in _stack)
            {
                frame.update();
            }
        }

        public void render()
        {
            foreach (Frame frame in _stack)
            {
                frame.render();
            }
        }

        public void unload()
        {
            while (_stack.Count > 0)
            {
                _stack[_stack.Count - 1].unload();
                _stack.RemoveAt(_stack.Count - 1);
            }
        }

        public void queuePush(Frame frame)
        {
            _actionQueue.Enqueue(new Action.Push(frame));
        }

        public void queuePop()
        {
            _actionQueue.Enqueue(new Action.Pop());
        }

        public void queueClear()
        {
            _actionQueue.Enqueue(new Action.Clear());
        }
    }
}
