﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSGraphics;

namespace Main
{
    public class GameDefaultFramebuffer : DefaultFramebuffer
    {
        private Window _window;

        public GameDefaultFramebuffer(Window window)
        {
            this._window = window;
        }

        public override int width
        {
            get
            {
                return _window.ClientSize.Width;
            }
        }

        public override int height
        {
            get
            {
                return _window.ClientSize.Height;
            }
        }
    }
}
