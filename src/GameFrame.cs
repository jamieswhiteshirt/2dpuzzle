﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Main;
using Entity;
using CSGraphics;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Main.Utility;
using Main.Render;

namespace Game
{
    class GameFrame : Frame
    {
        private World world = new World();
        private DoubleBuffered<List<Renderer>> _entityRenderers = new DoubleBuffered<List<Renderer>>(new List<Renderer>());
        private RenderContext _renderContext = new RenderContext();

        void Frame.load()
        {
            GL.Enable(EnableCap.Texture2D);
        }

        void Frame.unload()
        {

        }

        void Frame.update()
        {
            world.update();

            List<EntityBase> entities = world.getEntityList();
            List<Renderer> renderers = new List<Renderer>();
            for (int i = 0; i < entities.Count; i++)
            {
                Renderer renderer = entities[i].getRenderer();
                if (renderer != null)
                {
                    renderers.Add(renderer);
                }
            }
            _entityRenderers[0] = renderers;
            _entityRenderers.swap();
        }

        void Frame.render()
        {
            _renderContext.reset();

            _renderContext.projection.multiply(Matrix4.CreateOrthographicOffCenter(-5.0f, 5.0f, -5.0f, 5.0f, -1.0f, 1.0f));

            List<Renderer> renderers = _entityRenderers[1];
            for (int i = 0; i < renderers.Count; i++)
            {
                renderers[i].render(_renderContext, 1.0f);
            }
        }
    }
}
