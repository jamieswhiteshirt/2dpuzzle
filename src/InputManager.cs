﻿using System;
using OpenTK.Input;

namespace Input
{
    public class InputManager
    {
        private MouseState mouse;
        private KeyboardState keyboard;
        private bool[] keyTickStatus = new bool[131];
        private bool[] keyTickStatusPrevious = new bool[131];

        public InputManager()
        {
            mouse = Mouse.GetState();
            keyboard = Keyboard.GetState();
        }

        public void update()
        {
            mouse = Mouse.GetState();
            keyboard = Keyboard.GetState();
            keyTickStatusPrevious = keyTickStatus;
            keyTickStatus = new bool[131];
            for(int i = 0; i < 131; i++)
            {
                keyTickStatus[i] = keyboard[(Key)i];
            }
        }

        public bool isPressed(Key key)
        {
            int id = (int)key;
            return keyTickStatus[id] == true && keyTickStatusPrevious[id] == false;
        }

        public bool isDown(Key key)
        {
            return keyboard.IsKeyDown(key);
        }

        //This is the 'effects' stage. 
        public void tick()
        {
            if (mouse[MouseButton.Left])
            {
                Console.WriteLine("MOUSETEST");
            }
            if (isPressed(Key.W))
            {
                Console.WriteLine("KEYTEST");
            }
        }
    }
}