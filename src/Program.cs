﻿using OpenTK;
using OpenTK.Graphics;
using Game;
using CSGraphics;
using Input;

namespace Main
{
    static class Program
    {
        public static Window window;
        public static GameFrame gameFrame;
        public static ResourceLoader resourceLoader;

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                resourceLoader = new ResourceLoader();
            }
            else
            {
                resourceLoader = new ResourceLoader(args[0]);
            }
            ShaderTemplater.shaderSourceLoader = resourceLoader;

            window = new Window();
            gameFrame = new Game.GameFrame();
            window.frameStack.queuePush(gameFrame);

            window.Run(60, 60);
        }
    }
}
