﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using CSGraphics;

namespace Main.Render
{
    public static class CommonMeshes
    {
        public static VertexStream square;

        public static void initialize()
        {
            _initializeSquare();
        }

        private static void _initializeSquare()
        {
            square = new VertexStream();

            StaticAttributeData<Vector3> squareVertexData = new StaticAttributeData<Vector3>(BufferUsageHint.StaticDraw, 4);
            StaticAttributeData<Vector4> squareColorData = new StaticAttributeData<Vector4>(BufferUsageHint.StaticDraw, 4);
            StaticAttributeData<Vector2> squareTexCoordData = new StaticAttributeData<Vector2>(BufferUsageHint.StaticDraw, 4);

            squareVertexData.add(new Vector3(-1.0f, -1.0f, 0.0f));
            squareColorData.add(new Vector4(0.0f, 0.0f, 1.0f, 1.0f));
            squareTexCoordData.add(new Vector2(0.0f, 0.0f));

            squareVertexData.add(new Vector3(-1.0f,  1.0f, 0.0f));
            squareColorData.add(new Vector4(0.0f, 1.0f, 1.0f, 1.0f));
            squareTexCoordData.add(new Vector2(0.0f, 1.0f));

            squareVertexData.add(new Vector3( 1.0f,  1.0f, 0.0f));
            squareColorData.add(new Vector4(1.0f, 1.0f, 1.0f, 1.0f));
            squareTexCoordData.add(new Vector2(1.0f, 1.0f));

            squareVertexData.add(new Vector3( 1.0f, -1.0f, 0.0f));
            squareColorData.add(new Vector4(1.0f, 0.0f, 1.0f, 1.0f));
            squareTexCoordData.add(new Vector2(1.0f, 0.0f));

            StaticAttributeData<ushort> squareIndexData = new StaticAttributeData<ushort>(BufferUsageHint.StaticDraw, 6);

            squareIndexData.add(0);
            squareIndexData.add(1);
            squareIndexData.add(2);
            squareIndexData.add(0);
            squareIndexData.add(2);
            squareIndexData.add(3);

            square.addAttributePointer(new AttributePointer(0, 3, VertexAttribPointerType.Float, false, 0, 0, squareVertexData));
            square.addAttributePointer(new AttributePointer(1, 4, VertexAttribPointerType.Float, false, 0, 0, squareColorData));
            square.addAttributePointer(new AttributePointer(2, 2, VertexAttribPointerType.Float, false, 0, 0, squareTexCoordData));
            square.indexArray = squareIndexData;
        }
    }
}
