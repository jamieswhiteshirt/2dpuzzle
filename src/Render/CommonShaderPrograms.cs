﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using CSGraphics;

namespace Main.Render
{
    public static class CommonShaderPrograms
    {
        public static ShaderProgram texturedColored;

        public static void initialize()
        {
            initializeTexturedColored();
        }

        public static void initializeTexturedColored()
        {
            texturedColored = new ShaderProgram("default");
            texturedColored.bindShader(new VertexShader("default.vsh"));
            texturedColored.bindShader(new FragmentShader("default.fsh"));
        }
    }
}
