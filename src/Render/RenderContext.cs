﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSGraphics;
using OpenTK.Graphics.OpenGL;

namespace Main.Render
{
    public class RenderContext
    {
        public Matrix4Stack projection = new Matrix4Stack();
        public Matrix4Stack modelview = new Matrix4Stack();

        private ShaderProgram _boundShaderProgram;
        private Matrix4Uniform _projectionUniform;
        private Matrix4Uniform _modelviewUniform;

        public RenderContext()
        {

        }

        public void bindShaderProgram(ShaderProgram shaderProgram)
        {
            if (_boundShaderProgram != shaderProgram)
            {
                _boundShaderProgram = shaderProgram;

                _boundShaderProgram.bind();
                _modelviewUniform = _boundShaderProgram.getUniform<Matrix4Uniform>("modelview");
                _projectionUniform = _boundShaderProgram.getUniform<Matrix4Uniform>("projection");
            }
        }

        public void bindTexture(Texture texture)
        {
            texture.bind();
        }

        public void render(VertexStream vertexStream, int count = 0, int first = 0, PrimitiveType primitiveType = PrimitiveType.Triangles)
        {
            _projectionUniform.set(projection.get());
            _modelviewUniform.set(modelview.get());
            _boundShaderProgram.uploadUniforms();

            vertexStream.render(count, first, primitiveType);
        }

        public void reset()
        {
            projection.clear();
            modelview.clear();
        }
    }
}
