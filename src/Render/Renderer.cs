﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using Main.Utility;
using CSGraphics;

namespace Main.Render
{
    /// <summary>
    /// A short-lived object to render something between two ticks. Must copy the necessary data from its source to do so.
    /// </summary>
    public abstract class Renderer
    {
        protected DoubleBuffered<Vector2> _position;
        protected ShaderProgram _shaderProgram;

        public Renderer(DoubleBuffered<Vector2> position, ShaderProgram shaderProgram)
        {
            this._position = position;
            this._shaderProgram = shaderProgram;
        }

        public abstract void render(RenderContext context, float partialTick);
    }
}
