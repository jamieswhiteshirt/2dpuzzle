﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Main.Utility;
using OpenTK;
using CSGraphics;

namespace Main.Render
{
    public class SimpleMeshRenderer : Renderer
    {
        private readonly VertexStream _mesh;
        private readonly Texture2D _texture;
        private Matrix4Uniform _modelviewUniform;
        private Matrix4Uniform _projectionUniform;

        public SimpleMeshRenderer(DoubleBuffered<Vector2> position, ShaderProgram program, VertexStream mesh, Texture2D texture)
            : base(position, program)
        {
            this._mesh = mesh;
            this._texture = texture;

            _modelviewUniform = _shaderProgram.getUniform<Matrix4Uniform>("modelview");
            _projectionUniform = _shaderProgram.getUniform<Matrix4Uniform>("projection");
        }

        public override void render(RenderContext context, float partialTick)
        {
            Vector2 position = Interpolate.Vector2_.lerp(_position, partialTick);

            context.modelview.push(Matrix4.CreateTranslation(position.X, position.Y, 0.0f));

            context.bindShaderProgram(_shaderProgram);
            context.bindTexture(_texture);
            context.render(_mesh);

            context.modelview.pop();
        }
    }
}
