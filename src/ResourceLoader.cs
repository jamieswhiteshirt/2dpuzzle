﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSGraphics;
using System.IO;
using OpenTK.Graphics.OpenGL;
using System.Drawing;

namespace Main
{
    public class ResourceLoader : ShaderSourceLoader
    {
        public readonly String assetsDirectory;
        private Dictionary<string, Texture2D> textureMap = new Dictionary<string, Texture2D>();

        public ResourceLoader()
        {
            assetsDirectory = new FileInfo(System.Reflection.Assembly.GetEntryAssembly().Location).Directory + @"\assets\";
        }

        public ResourceLoader(String assetsDirectory)
        {
            this.assetsDirectory = assetsDirectory;
        }

        String ShaderSourceLoader.getShaderSource(String shaderName)
        {
            String filePath = assetsDirectory + shaderName;

            if (File.Exists(filePath))
            {
                return File.ReadAllText(filePath);
            }
            else
            {
                throw new FileNotFoundException(String.Format("Shader file {0} could not be found in asset directory {1}", shaderName, assetsDirectory));
            }
        }

        String ShaderSourceLoader.getShaderSource(String shaderName, String parentShaderName)
        {
            return (this as ShaderSourceLoader).getShaderSource(shaderName);
        }

        public Texture2D getTexture(string textureName)
        {
            Texture2D texture = null;
            if(textureMap.ContainsKey(textureName))
            {
                texture = textureMap[textureName];
            }
            else
            {
                texture = new Texture2D(this.getBitmap(textureName), TextureTarget.Texture2D, TextureMagFilter.Linear, TextureMinFilter.Linear, TextureWrapMode.ClampToEdge);
                textureMap.Add(textureName, texture);
            }
            return texture;
        }

        public Bitmap getBitmap(String name)
        {
            return new Bitmap(assetsDirectory + name);
        }
    }
}
