﻿using Main;
using Main.Render;
using Main.Utility;
using OpenTK;
using System;

namespace Terrain
{
    public class TerrainBase
    {
        public readonly int terrainID;
        public World world;

        public Vector2 position;

        private string _textureName;
        private Random _random = new Random();

        public TerrainBase(World world, Vector2 position)
        {
            this.world = world;
            this.terrainID = world.getNewTerrainID(this);

            this.position = new Vector2();
        }

        public TerrainBase setPosition(float x, float y)
        {
            this.position = new Vector2(x, y);
            return this;
        }

        public TerrainBase setPosition(Vector2 position)
        {
            this.position = position;
            return this;
        }

        public TerrainBase setTextureName(string textureName)
        {
            this._textureName = textureName;
            return this;
        }

        public virtual void update()
        {

        }
    }
}
