﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main.Utility
{
    public struct DoubleBuffered<T>
    {
        private readonly T[] _values;

        public DoubleBuffered(T value)
        {
            _values = new T[]{ value, value };
        }

        public void swap()
        {
            T temp = _values[0];
            _values[0] = _values[1];
            _values[1] = temp;
        }

        public T this[int index]
        {
            get
            {
                return _values[index];
            }
            set
            {
                _values[index] = value;
            }
        }
    }
}
