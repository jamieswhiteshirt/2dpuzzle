﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Main.Utility
{
    /// <summary>
    /// Utility class for Interpolation.
    /// Every interpolation function goes from second to first.
    /// If t == 0.0f, the result is second. If t == 1.0f, the result is first.
    /// If a DoubleBuffered is used, the same rule applies.
    /// Every interpolation type uses a singleton.
    /// </summary>
    public class Interpolate
    {
        public abstract class InterpolateBase<T>
        {
            public abstract T sum(T first, float t1, T second, float t2);

            public T lerp(T first, T second, float t)
            {
                return sum(first, t, second, 1.0f - t);
            }

            public T lerp(DoubleBuffered<T> values, float t)
            {
                return lerp(values[0], values[1], t);
            }
        }

        private class InterpolateVector2 : InterpolateBase<Vector2>
        {
            public override Vector2 sum(Vector2 first, float t1, Vector2 second, float t2)
            {
                return first * t1 + second * t2;
            }
        }

        public static InterpolateBase<Vector2> Vector2_ = new InterpolateVector2();
    }
}
