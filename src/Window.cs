﻿using System;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Collections.Generic;
using Main.Render;
using Input;

namespace Main
{
    public class Window : GameWindow
    {
        public FrameStack frameStack = new FrameStack();
        public GameDefaultFramebuffer defaultFramebuffer;
        public InputManager inputManager;

        public Window()
            : base(500, 500, new GraphicsMode(32, 24, 8, 4), "2DPuzzle", GameWindowFlags.Default, DisplayDevice.Default, 4, 0, GraphicsContextFlags.ForwardCompatible)
        {
            defaultFramebuffer = new GameDefaultFramebuffer(this);
            inputManager = new InputManager();
        }

        protected override void OnLoad(EventArgs args)
        {
            base.OnLoad(args);

            CommonMeshes.initialize();
            CommonShaderPrograms.initialize();
        }

        protected override void OnResize(EventArgs args)
        {
            base.OnResize(args);
        }

        protected override void OnUpdateFrame(FrameEventArgs args)
        {
            base.OnUpdateFrame(args);

            inputManager.tick();
            inputManager.update();

            frameStack.update();
        }

        protected override void OnRenderFrame(FrameEventArgs args)
        {
            base.OnRenderFrame(args);

            defaultFramebuffer.use();
            frameStack.render();

            //GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            SwapBuffers();
        }

        protected override void OnClosed(EventArgs args)
        {
            base.OnClosed(args);


        }
    }
}
