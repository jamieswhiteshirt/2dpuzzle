﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
using OpenTK;
using Terrain;

namespace Main
{
    public class World
    {
        private static int _entityIDCounter;
        private static int _terrainIDCounter;
        private Dictionary<int, EntityBase> _entities = new Dictionary<int, EntityBase>();
        private Dictionary<int, TerrainBase> _terrain = new Dictionary<int, TerrainBase>();

        public World()
        {
            new EntityBase(this, new Vector2(0.0f, 0.0f)).setTextureName("test.png");
        }

        public void update()
        {
            List<EntityBase> copyOfEntities = getEntityList();
            List<TerrainBase> copyOfTerrain = getTerrainList();
            for (int i = 0; i < copyOfEntities.Count; i++) 
            {
                if (copyOfEntities[i] != null) 
                {
                    copyOfEntities[i].update ();
                }
            }
            for (int i = 0; i < copyOfTerrain.Count; i++)
            {
                if (copyOfTerrain[i] != null)
                {
                    copyOfTerrain[i].update();
                }
            }
        }

        public int getNewTerrainID(TerrainBase entity)
        {
            int terrainID = _terrainIDCounter++;
            _terrain[terrainID] = entity;
            return terrainID;
        }

        public List<TerrainBase> getTerrainList()
        {
            return _terrain.Values.ToList();
        }

        public int getNewEntityID(EntityBase entity)
        {
            int entityID = _entityIDCounter++;
            _entities[entityID] = entity;
            return entityID;
        }

        public List<EntityBase> getEntityList()
        {
            return _entities.Values.ToList();
        }
    }
}
